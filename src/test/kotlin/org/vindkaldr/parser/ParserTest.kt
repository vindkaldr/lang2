package org.vindkaldr.parser

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Test
import org.vindkaldr.lexer.Lexer

class ParserTest {
    @Test
    fun `report expected print`() {
        assertErrors("", "EXPECTED_PRINT:1.1")
        assertErrors("42", "EXPECTED_PRINT:1.1")
        assertErrors("print 42 p", "EXPECTED_PRINT:1.10")
    }

    @Test
    fun `report expected number`() {
        assertErrors("print", "EXPECTED_NUMBER:1.6")
        assertErrors("print print", "EXPECTED_NUMBER:1.7")
        assertErrors("print 42 print", "EXPECTED_NUMBER:1.15")
    }

    @Test
    fun `report expected print and number`() {
        assertErrors("42 print", "EXPECTED_PRINT:1.1,EXPECTED_NUMBER:1.9")
    }

    @Test
    fun `report expected prints`() {
        assertErrors("42 42", "EXPECTED_PRINT:1.1,EXPECTED_PRINT:1.4")
    }

    @Test
    fun `build model`() {
        assertModel("print 42", "print:42")
        assertModel("print 42 print 666", "print:42,print:666")
    }

    private fun assertErrors(sourceCode: String, expectedModel: String) {
        val parserModelBuilder = DefaultParserModelBuilder()
        val lexer = Lexer(Parser(parserModelBuilder))

        lexer.lex(sourceCode)

        assert.that(parserModelBuilder.parserModel.getErrors(), equalTo(expectedModel))
    }

    private fun ParserModel.getErrors(): String {
        return syntaxErrors.joinToString(separator = ",") {
            "${it.type}:${it.lineNumber}.${it.position}"
        }
    }

    private fun assertModel(sourceCode: String, expectedModel: String) {
        val parserModelBuilder = DefaultParserModelBuilder()
        val lexer = Lexer(Parser(parserModelBuilder))

        lexer.lex(sourceCode)

        assert.that(parserModelBuilder.parserModel.getModel(), equalTo(expectedModel))
    }

    private fun ParserModel.getModel(): String {
        return printStatements.joinToString(separator = ",") { "print:${it.value}" }
    }
}
