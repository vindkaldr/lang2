package org.vindkaldr.lexer

import org.junit.Test

class LexerPrintPositionTest : LexerTestBase() {
    override fun print(lineNumber: Int, position: Int) {
        addToken("print:$lineNumber.$position")
    }

    @Test
    fun `print token with position at the beginning of the first line`() {
        assertToken("print", "print:1.1,eof")
    }

    @Test
    fun `print token with position in the first line`() {
        assertToken("1024 print", "number,print:1.6,eof")
    }

    @Test
    fun `print token with position at the beginning of the second line`() {
        assertToken("\nprint", "print:2.1,eof")
    }

    @Test
    fun `print token with position in the second line`() {
        assertToken("\n1024 print", "number,print:2.6,eof")
    }
}
