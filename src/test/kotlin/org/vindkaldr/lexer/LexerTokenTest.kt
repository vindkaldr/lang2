package org.vindkaldr.lexer

import org.junit.Test

class LexerTokenTest : LexerTestBase() {
    @Test
    fun `eof token for empty string`() {
        assertToken("", "eof")
    }

    @Test
    fun `print token for print statement`() {
        assertToken("print", "print,eof")
    }

    @Test
    fun `number token for single digit number literal`() {
        assertToken("1", "number,eof")
    }

    @Test
    fun `number token for three digit number literal`() {
        assertToken("666", "number,eof")
    }

    @Test
    fun `print and number token for print statement with number literal`() {
        assertToken("print 42", "print,number,eof")
    }

    @Test
    fun `multiple print and number token for print statements with number literals`() {
        assertToken("print 42 print 24", "print,number,print,number,eof")
    }

    @Test
    fun `ignore spaces and tabs`() {
        assertToken("  \tprint\t\t42\t ", "print,number,eof")
    }

    @Test
    fun `ignore line ends on linux`() {
        assertToken("\nprint\n\n42\n", "print,number,eof")
    }

    @Test
    fun `ignore line ends on windows`() {
        assertToken("\r\nprint\r\n\r\n42\r\n", "print,number,eof")
    }

    @Test
    fun `ignore mixed line ends`() {
        assertToken("\r\nprint\n\r\n42\n", "print,number,eof")
    }
}
