package org.vindkaldr.lexer

import org.junit.Test

class LexerErrorPositionTest : LexerTestBase() {
    override fun error(lineNumber: Int, position: Int) {
        addToken("error:$lineNumber.$position")
    }

    @Test
    fun `error with position at the beginning of the first line`() {
        assertToken("readLine", "error:1.1,eof")
    }

    @Test
    fun `error with position in the first line`() {
        assertToken("print readLine", "print,error:1.7,eof")
    }

    @Test
    fun `error with position at the beginning of the second line`() {
        assertToken("\nreadLine", "error:2.1,eof")
    }

    @Test
    fun `error with position in the second line`() {
        assertToken("\nprint readLine", "print,error:2.7,eof")
    }
}
