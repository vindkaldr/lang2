package org.vindkaldr.lexer

import org.junit.Test

class LexerNumberValueTest : LexerTestBase() {
    override fun number(value: String, lineNumber: Int, position: Int) {
        addToken("number:$value")
    }

    @Test
    fun `number token with number value for number literal`() {
        assertToken("1024", "number:1024,eof")
    }
}
