package org.vindkaldr.lexer

import com.natpryce.hamkrest.assertion.assert
import com.natpryce.hamkrest.equalTo
import org.junit.Before

open class LexerTestBase : TokenCollector {
    private lateinit var lexer: Lexer
    private lateinit var tokens: String

    @Before
    fun setUp() {
        lexer = Lexer(this)
        tokens = ""
    }

    override fun endOfFile(lineNumber: Int, position: Int) {
        addToken("eof")
    }

    override fun print(lineNumber: Int, position: Int) {
        addToken("print")
    }

    override fun number(value: String, lineNumber: Int, position: Int) {
        addToken("number")
    }

    override fun error(lineNumber: Int, position: Int) {
        addToken("error")
    }

    protected fun addToken(token: String) {
        tokens += if (tokens.isBlank()) token else ",$token"
    }

    protected fun assertToken(sourceCode: String, expectedTokens: String) {
        lexer.lex(sourceCode)
        assert.that(tokens, equalTo(expectedTokens))
    }
}
