package org.vindkaldr.lexer

import org.junit.Test

class LexerLineEndPositionTest : LexerTestBase() {
    override fun endOfFile(lineNumber: Int, position: Int) {
        addToken("eof:$lineNumber.$position")
    }

    @Test
    fun `eof token with position at the beginning of the first line`() {
        assertToken("", "eof:1.1")
    }

    @Test
    fun `eof token with position in the first line`() {
        assertToken("print", "print,eof:1.6")
    }

    @Test
    fun `eof token with position at the beginning of the second line`() {
        assertToken("\n", "eof:2.1")
    }

    @Test
    fun `eof token with position in the second line`() {
        assertToken("\nprint", "print,eof:2.6")
    }
}
