package org.vindkaldr.lexer

import org.junit.Test

class LexerErrorTest : LexerTestBase() {
    @Test
    fun `error for not existing statement`() {
        assertToken("readLine", "error,eof")
    }

    @Test
    fun `error for incorrect print statement`() {
        assertToken("printLine", "error,eof")
    }

    @Test
    fun `token after error`() {
        assertToken("printt print", "error,print,eof")
    }
}
