package org.vindkaldr.lexer

import org.junit.Test

class LexerNumberPositionTest : LexerTestBase() {
    override fun number(value: String, lineNumber: Int, position: Int) {
        addToken("number:$lineNumber.$position")
    }

    @Test
    fun `number token with position at the beginning of the first line`() {
        assertToken("1024", "number:1.1,eof")
    }

    @Test
    fun `number token with position in the first line`() {
        assertToken("print 1024", "print,number:1.7,eof")
    }

    @Test
    fun `number token with position at the beginning of the second line`() {
        assertToken("\n1024", "number:2.1,eof")
    }

    @Test
    fun `number token with position in the second line`() {
        assertToken("\nprint 1024", "print,number:2.7,eof")
    }
}
