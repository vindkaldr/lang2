package org.vindkaldr.lexer

interface TokenCollector {
    fun endOfFile(lineNumber: Int, position: Int)
    fun print(lineNumber: Int, position: Int)
    fun number(value: String, lineNumber: Int, position: Int)
    fun error(lineNumber: Int, position: Int)
}
