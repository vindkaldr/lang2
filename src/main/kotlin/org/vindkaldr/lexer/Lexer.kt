package org.vindkaldr.lexer

import java.util.regex.Pattern

private const val PRINT_STATEMENT = "print"

private val spaceOrTabPattern = Pattern.compile("^[ \t]+")
private val lineEndPattern = Pattern.compile("^(\\n|\\r\\n)+")
private val numberPattern = Pattern.compile("^\\d+")
private val printPattern = Pattern.compile("^$PRINT_STATEMENT$|^$PRINT_STATEMENT\\s")
private val wordPattern = Pattern.compile("^(\\S+)$|^(\\S+)\\s")

class Lexer(private val tokenCollector: TokenCollector) {
    private var overallPosition = 0
    private var currentLineNumber = 1
    private var currentPosition = 1
    private lateinit var remainingSourceCode: String

    fun lex(sourceCode: String) {
        while (!isEndOfSourceCodeReached(sourceCode)) {
            updateRemainingSourceCode(sourceCode)

            if (!findToken()) {
                tokenCollector.error(currentLineNumber, currentPosition)
                skipWord()
            }
        }
        tokenCollector.endOfFile(currentLineNumber, currentPosition)
    }

    private fun isEndOfSourceCodeReached(sourceCode: String): Boolean {
        return sourceCode.lastIndex < overallPosition
    }

    private fun updateRemainingSourceCode(sourceCode: String) {
        remainingSourceCode = sourceCode.substring(overallPosition)
    }

    private fun skipWord() {
        val wordMatcher = wordPattern.matcher(remainingSourceCode)
        wordMatcher.find()
        val nextWordLength = wordMatcher.group().length
        overallPosition += nextWordLength
        currentPosition += nextWordLength
    }

    private fun findToken(): Boolean {
        return findLineEnd() ||
                findSpaceOrTab() ||
                findPrint() ||
                findNumber()
    }

    private fun findLineEnd(): Boolean {
        val lineEndMatcher = lineEndPattern.matcher(remainingSourceCode)
        if (lineEndMatcher.find()) {
            overallPosition += lineEndMatcher.group().length
            currentLineNumber++
            currentPosition = 1
            return true
        }
        return false
    }

    private fun findSpaceOrTab():Boolean {
        val spaceOrTabMatcher = spaceOrTabPattern.matcher(remainingSourceCode)
        if (spaceOrTabMatcher.find()) {
            overallPosition += spaceOrTabMatcher.group().length
            currentPosition += spaceOrTabMatcher.group().length
            return true
        }
        return false
    }

    private fun findPrint(): Boolean {
        val printMatcher = printPattern.matcher(remainingSourceCode)
        if (printMatcher.find()) {
            tokenCollector.print(currentLineNumber, currentPosition)
            overallPosition += PRINT_STATEMENT.length
            currentPosition += PRINT_STATEMENT.length
            return true
        }
        return false
    }

    private fun findNumber(): Boolean {
        val numberMatcher = numberPattern.matcher(remainingSourceCode)
        if (numberMatcher.find()) {
            val number = numberMatcher.group()
            tokenCollector.number(number, currentLineNumber, currentPosition)
            overallPosition += number.length
            currentPosition += number.length
            return true
        }
        return false
    }
}
