package org.vindkaldr.parser

class DefaultParserModelBuilder : ParserModelBuilder {
    val parserModel = ParserModel()
    private lateinit var numberValue: String

    override fun setNumberValue(value: String) {
        numberValue = value
    }

    override fun addPrintStatement() {
        parserModel.printStatements.add(PrintStatement(numberValue))
    }

    override fun syntaxError(lineNumber: Int, position: Int) {
        parserModel.syntaxErrors.add(SyntaxError(SyntaxError.Type.SYNTAX, lineNumber, position))
    }

    override fun expectedPrint(lineNumber: Int, position: Int) {
        parserModel.syntaxErrors.add(SyntaxError(SyntaxError.Type.EXPECTED_PRINT, lineNumber, position))
    }

    override fun expectedNumber(lineNumber: Int, position: Int) {
        parserModel.syntaxErrors.add(SyntaxError(SyntaxError.Type.EXPECTED_NUMBER, lineNumber, position))
    }
}
