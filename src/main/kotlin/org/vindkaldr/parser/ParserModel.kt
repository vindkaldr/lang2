package org.vindkaldr.parser

class ParserModel(
    var printStatements: MutableList<PrintStatement> = mutableListOf(),
    var syntaxErrors: MutableList<SyntaxError> = mutableListOf()
)

class PrintStatement(var value: String)

class SyntaxError(val type: Type, val lineNumber: Int, val position: Int) {
    enum class Type {
        SYNTAX, EXPECTED_PRINT, EXPECTED_NUMBER
    }
}

