package org.vindkaldr.parser

import org.vindkaldr.lexer.TokenCollector

/**
 * <program> ::= <print-statement>*
 * <print-statement> ::= "print" <number>
 */

private val PARSER_TRANSITIONS = listOf(
    Transition(State.BEGIN, Event.PRINT, State.PRINT),
    Transition(State.PRINT, Event.NUMBER, State.NUMBER, ParserModelBuilder::addPrintStatement),
    Transition(State.NUMBER, Event.PRINT, State.PRINT),
    Transition(State.NUMBER, Event.END_OF_FILE, State.END)
)

private class Transition(
    val state: State,
    val event: Event,
    val nextState: State,
    val action: (builder: ParserModelBuilder) -> Unit = {}
)

private enum class State {
    BEGIN, PRINT, NUMBER, END
}

private enum class Event {
    PRINT, NUMBER, END_OF_FILE
}

class Parser(private val parserModelBuilder: ParserModelBuilder) : TokenCollector {
    private var currentState = State.BEGIN

    override fun endOfFile(lineNumber: Int, position: Int) {
        handleEvent(Event.END_OF_FILE, lineNumber, position)
    }

    override fun print(lineNumber: Int, position: Int) {
        handleEvent(Event.PRINT, lineNumber, position)
    }

    override fun number(value: String, lineNumber: Int, position: Int) {
        parserModelBuilder.setNumberValue(value)
        handleEvent(Event.NUMBER, lineNumber, position)
    }

    override fun error(lineNumber: Int, position: Int) {
        handleErrorEvent(lineNumber, position)
    }

    private fun handleEvent(event: Event, lineNumber: Int, position: Int) {
        val transition = getTransition(event)
        if (transition != null) {
            handleTransition(transition)
        }
        else {
            handleErrorEvent(lineNumber, position)
        }
    }

    private fun getTransition(event: Event): Transition? {
        return PARSER_TRANSITIONS.firstOrNull { it.state == currentState && it.event == event }
    }

    private fun handleTransition(transition: Transition) {
        currentState = transition.nextState
        transition.action(parserModelBuilder)
    }

    private fun handleErrorEvent(lineNumber: Int, position: Int) {
        when (currentState) {
            State.BEGIN -> {
                parserModelBuilder.expectedPrint(lineNumber, position)
                currentState = State.NUMBER
            }
            State.PRINT -> {
                parserModelBuilder.expectedNumber(lineNumber, position)
                currentState = State.NUMBER
            }
            State.NUMBER -> {
                parserModelBuilder.expectedPrint(lineNumber, position)
                currentState = State.NUMBER
            }
            else -> parserModelBuilder.syntaxError(lineNumber, position)
        }
    }
}
