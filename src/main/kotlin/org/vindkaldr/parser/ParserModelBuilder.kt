package org.vindkaldr.parser

interface ParserModelBuilder {
    fun setNumberValue(value: String)
    fun addPrintStatement()

    fun syntaxError(lineNumber: Int, position: Int)
    fun expectedPrint(lineNumber: Int, position: Int)
    fun expectedNumber(lineNumber: Int, position: Int)
}
